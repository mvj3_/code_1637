package com.allen.progress.bar;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ProgressActivity extends Activity {
    /** Called when the activity is first created. */
	
	private MyDialgaugeView myDialgaugeView = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        myDialgaugeView = (MyDialgaugeView)findViewById(R.id.mydialgaugeview);
        
        myDialgaugeView.setPos(90);
        
        Button btn = (Button)findViewById(R.id.btn);
        btn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Toast.makeText(ProgressActivity.this, myDialgaugeView.getPos()+"", 2000).show();
				
			}
		});
        
    }
}