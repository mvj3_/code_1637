package com.allen.progress.bar;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class MyDialgaugeView extends LinearLayout {

	private SeekBar seekBar1;
	private SeekBar seekBar2;
	private Context mContext;

	public MyDialgaugeView(Context context, AttributeSet attrs) {
		super(context,attrs);
		mContext = context;
		LayoutInflater.from(mContext).inflate(R.layout.mydialgauge, this, true);

		init();
	}

	private void init() {
		seekBar1 = (SeekBar)findViewById(R.id.seekbar1);
		seekBar1.setThumb(null);
		seekBar1.setMax(100);
		seekBar1.setProgress(0);
		seekBar1.setClickable(false);
		seekBar1.setOnSeekBarChangeListener(listener1);
		seekBar2 = (SeekBar)findViewById(R.id.seekbar2);
		seekBar2.setProgress(100);
		Log.e("test", "---");
		
	}

	public int getPos() {
		return seekBar2.getProgress();
	}

	public void setPos(int progress) {
		seekBar1.setProgress(progress);
		Log.e("test getProgress", ""+ seekBar1.getProgress());
	}

	OnSeekBarChangeListener listener1 = new OnSeekBarChangeListener() {
		int originalProgress;

		public void onStopTrackingTouch(SeekBar seekBar) {
		}

		public void onStartTrackingTouch(SeekBar seekBar) {
			originalProgress = seekBar.getProgress();
		}

		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			if (fromUser == true) {

				seekBar.setProgress(originalProgress);

			}
		}
	};

}
